﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace PageModeCSharp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public SerialPort seriaPort = new SerialPort();

        private void button1_Click(object sender, EventArgs e)
        {
            int baudRate = 115200;
            int dataBits = 8;
            Handshake handshake = Handshake.None;
            Parity parity = Parity.None;
            string portName = textBox1.Text;
            StopBits stopBits = StopBits.One;

            seriaPort.BaudRate = baudRate;
            seriaPort.DataBits = dataBits;
            seriaPort.Parity = parity;
            seriaPort.PortName = portName;
            seriaPort.StopBits = stopBits;
            seriaPort.Open();

            if (seriaPort.IsOpen == true)
            {
                textBox2.Text = "Conectado com Sucesso.";
            }
            else
            {
                textBox2.Text = "Erro na Conexao.";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // Limpando o Buffer de Impressão.
            byte[] comando = { 0x1B, 0x40 };
            seriaPort.Write(comando, 0, comando.Length);
            // Centralizando o Texto.
            comando = new byte[] { 0x1B, 0x61, 0x1 };
            seriaPort.Write(comando, 0, comando.Length);
            // Definindo tipo e tamanho da fonte.
            comando = new byte[] { 0x1B, 0x21, 0x1 };
            seriaPort.Write(comando, 0, comando.Length);
            // Mandar Impressão.
            String texto = "Elgin Manaus\n";
            seriaPort.Write(texto);
            texto = "Elgin/SA\n";
            seriaPort.Write(texto);
            texto = "Rua Abiurana, 579 Distrito Industrial Manaus - AM\n\n";
            seriaPort.Write(texto);
            texto = "CNPJ 14.200.166/0001-66 IE 111.111.111.111 IM\n";
            seriaPort.Write(texto);
            comando = new byte[] { 0x1B, 0x40 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x61, 0x1 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x21, 0x1 };
            seriaPort.Write(comando, 0, comando.Length);
            texto = "----------------------------------------------------------------\n";
            seriaPort.Write(texto);
            comando = new byte[] { 0x1B, 0x21, 0x0 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x61, 0x1 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x21, 0x08 };
            seriaPort.Write(comando, 0, comando.Length);
            texto = "EXTRATO No. 002046\n";
            seriaPort.Write(texto);
            texto = "CUPOM FISCAL ELETRONICO - SAT\n";
            seriaPort.Write(texto);
            comando = new byte[] { 0x1B, 0x40 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x61, 0x1 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x21, 0x1 };
            seriaPort.Write(comando, 0, comando.Length);
            texto = "----------------------------------------------------------------\n";
            seriaPort.Write(texto);
            comando = new byte[] { 0x1B, 0x40 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x61, 0x0 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x21, 0x1 };
            seriaPort.Write(comando, 0, comando.Length);
            texto = "CPF/CNPJ consumidor: \n";
            seriaPort.Write(texto);
            comando = new byte[] { 0x1B, 0x61, 0x0 };
            seriaPort.Write(comando, 0, comando.Length);
            texto = "----------------------------------------------------------------\n";
            seriaPort.Write(texto);
            texto = "# | COD | DESC | QTD | UN | VL UN R$ | (VL TR R$)* | VL ITEM R$\n";
            seriaPort.Write(texto);
            texto = "----------------------------------------------------------------\n";
            seriaPort.Write(texto);
            texto = "1 0000000 IMPRESSORA NÃO FISCAL I9/I7 kg 1 KG X 1.290     10,00\n\n";
            seriaPort.Write(texto);
            comando = new byte[] { 0x1B, 0x40 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x61, 0x0 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x21, 0x1 };
            seriaPort.Write(comando, 0, comando.Length);
            texto = "TOTAL R$                                                  10,00\n";
            seriaPort.Write(texto);
            texto = "Dinheiro                                                  10,00\n";
            seriaPort.Write(texto);
            texto = "Troco R$                                                   0,00\n\n";
            seriaPort.Write(texto);
            texto = "----------------------------------------------------------------\n";
            seriaPort.Write(texto);
            texto = "Tributos Totais (Lei Fed 12.741/12) R$                      3,85\n";
            seriaPort.Write(texto);
            texto = "----------------------------------------------------------------\n";
            seriaPort.Write(texto);
            comando = new byte[] { 0x1B, 0x40 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x61, 0x0 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x21, 0x1 };
            seriaPort.Write(comando, 0, comando.Length);
            texto = "R$ 3,84 Trib aprox R$ 1,35 Fed R$ 2,50 Est\n";
            seriaPort.Write(texto);
            texto = "Fonte: IBPT/FECOMECIO (RS) 9oi3aC\n\n";
            seriaPort.Write(texto);
            texto = "KM: 0\n";
            seriaPort.Write(texto);
            comando = new byte[] { 0x1B, 0x40 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x61, 0x1 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x21, 0x1 };
            seriaPort.Write(comando, 0, comando.Length);
            texto = "----------------------------------------------------------------\n";
            seriaPort.Write(texto);
            comando = new byte[] { 0x1B, 0x40 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x61, 0x0 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x21, 0x1 };
            seriaPort.Write(comando, 0, comando.Length);
            texto = "* Valor aproximado dos tributos do item\n";
            seriaPort.Write(texto);
            comando = new byte[] { 0x1B, 0x40 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x61, 0x1 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x21, 0x1 };
            seriaPort.Write(comando, 0, comando.Length);
            texto = "----------------------------------------------------------------\n";
            seriaPort.Write(texto);
            comando = new byte[] { 0x1B, 0x40 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x21, 0x0 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x61, 0x1 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x21, 0x08 };
            seriaPort.Write(comando, 0, comando.Length);
            texto = "SAT No. 900001231\n";
            seriaPort.Write(texto);
            comando = new byte[] { 0x1B, 0x40 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x61, 0x1 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x21, 0x1 };
            texto = "00:00:00 - 00/00/0000\n\n";
            seriaPort.Write(texto);
            comando = new byte[] { 0x1B, 0x40 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x61, 0x1 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1B, 0x21, 0x1 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1D, 0x68, 70 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1D, 0x77, 1 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1D, 0x48, 1 };
            seriaPort.Write(comando, 0, comando.Length);
            String code128 = "{B35150661099008000141593515066109900800014159";
            int tamanho = code128.Length;
            comando = new byte[] { 0x1D, 0x6B, 0x49, (byte)tamanho };
            seriaPort.Write(comando, 0, comando.Length);
            seriaPort.Write(code128);
            comando = new byte[] { 0xA, 0xA };
            seriaPort.Write(comando, 0, comando.Length);
            int pl;
            int ph;
            String qrCode = "https://www.google.com.br/search?source=hp&ei=eaAeWq_VHIWowATBgI_ICA&q=netflix&oq=&gs_l=psy-ab.1.2.35i39k1l6.3560544.3562658.0.3569304.8.4.2.0.0.0.133.325.2j1.4.0....0...1c.1.64.psy-ab..2.6.460.6..0j0i131k1.101.ioMh3zF2gmoxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxrubensborgesdeandrade";
            int taman = qrCode.Length + 3;
            if(taman >= 255)
            {
                pl = taman % 256;
                ph = taman / 256;
            }
            else
            {
                pl = taman;
                ph = 0;
            }
            comando = new byte[] { 27, 97, 49 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 29, 40, 107, 3, 0, 49, 67, 5 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 29, 40, 107, 3, 0, 49, 69, 48 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1D, 0x28, 0x6B, (byte)pl, (byte)ph, 0x31, 0x50, 0x30 };
            seriaPort.Write(comando, 0, comando.Length);
            seriaPort.Write(qrCode);
            comando = new byte[] { 29, 40, 107, 3, 0, 49, 81, 48 };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0xA };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1D, 0x56, 0x48, 0x1D, 0x56, 0x42, 0x00 };
            seriaPort.Write(comando, 0, comando.Length);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // Limpar Buffer.
            byte[] comando = { 0x18 };
            seriaPort.Write(comando, 0, comando.Length);
            // Definir Posição de impressão do cupom inteiro na Horizontal.
            comando = new byte[] { 0x1B, 0x24, 24, 00 };
            seriaPort.Write(comando, 0, comando.Length);
            // Definir Posição da impressão do cupom inteiro na Vertical.
            comando = new byte[] { 0x1D, 0x24, 0x10, 0x00 };
            seriaPort.Write(comando, 0, comando.Length);
            // Seleção do Modo Página.
            comando = new byte[] { 0x1B, 0x4C };
            seriaPort.Write(comando, 0, comando.Length);
            // Definição da área Completa de Impressão.
            comando = new byte[] { 0x1B, 0x57, 0x00, 0x00, 0x00, 0x00, 0x48, 0x02, 24, 06 };
            seriaPort.Write(comando, 0, comando.Length);
            // Definição da direção de impressão - Modo Página.
            comando = new byte[] { 0x1B, 0x54, 0x00 };
            seriaPort.Write(comando, 0, comando.Length);
            // DEFINIÇÃO DA POSIÇÃO DE IMPRESSÃO NA HORIZONTAL.
            comando = new byte[] { 0x1B, 0x24, (byte)0x85, 0x00 };
            seriaPort.Write(comando, 0, comando.Length);
            // DEFINIÇÃO DA POSIÇÃO DE IMPRESSÃO NA VERTICAL.
            comando = new byte[] { 0x1D, 0x24, 0x10, 0x00 };
            seriaPort.Write(comando, 0, comando.Length);
            // DEFININDO TIPO E TAMANHO DA FONTE.
            comando = new byte[] { 0x1B, 0x21, 0x1 };
            seriaPort.Write(comando, 0, comando.Length);

            int pl2;
            int ph2;
            String qrcode2 = "https://www.google.com.br/search?source=hp&ei=eaAeWq_VHIWowATBgI_ICA&q=netflix&oq=&gs_l=psy-ab.1.2.35i39k1l6.3560544.3562658.0.3569304.8.4.2.0.0.0.133.325.2j1.4.0....0...1c.1.64.psy-ab..2.6.460.6..0j0i131k1.101.ioMh3zF2gmorubensborgesdeandrade";
            int taman2 = qrcode2.Length + 6;

            if (taman2 >= 255)
            {
                pl2 = taman2 % 256;
                ph2 = taman2 / 256;

            }
            else
            {

                pl2 = taman2;
                ph2 = 0;
            }
            // Posição da Impressao.
            comando = new byte[] { 0x1B, 0x24, 0x28, 0x00 };
            seriaPort.Write(comando, 0, comando.Length);
            // Definição do Tamanho do QRCode.
            comando = new byte[] { 29, 40, 107, 3, 0, 49, 67, 3 };
            seriaPort.Write(comando, 0, comando.Length);
            // Definição do Nível de Correção.
            comando = new byte[] { 29, 40, 107, 3, 0, 49, 69, 48 };
            seriaPort.Write(comando, 0, comando.Length);
            // Armazenamento de Dados do QRCode.
            comando = new byte[] { 0x1D, 0x28, 0x6B, (byte)pl2, (byte)ph2, 0x31, 0x50, 0x30 };
            seriaPort.Write(comando, 0, comando.Length);
            // Envio da Impressão.
            comando = new byte[] { 0x1B, 0x21, 0x1 };
            seriaPort.Write(comando, 0, comando.Length);
            seriaPort.Write(qrcode2);
            comando = new byte[] { 29, 40, 107, 3, 0, 49, 81, 48 };
            seriaPort.Write(comando, 0, comando.Length);
            // Definir Impresão Horizontal.
            comando = new byte[] { 0x1B, 0x24, 150, 0x00 };
            seriaPort.Write(comando, 0, comando.Length);
            // Tipo e tamanho da Impressão.
            comando = new byte[] { 0x1B, 0x21, 0x00 };
            seriaPort.Write(comando, 0, comando.Length);
            // Posição da Vertical.
            comando = new byte[] { 0x1D, 0x24, 0, 0 };
            seriaPort.Write(comando, 0, comando.Length);
            // Definir posição da impressão de texto.
            comando = new byte[] { 0x1B, 0x24, 235, 0x00 };
            seriaPort.Write(comando, 0, comando.Length);
            // Negrito.
            comando = new byte[] { 0x1B, 0x45, 0x01 };
            seriaPort.Write(comando, 0, comando.Length);
            String texto = "EXEMPLO DE IMPRESSAO\n";
            seriaPort.Write(texto);
            // Definir posição da impressão de texto.
            comando = new byte[] { 0x1B, 0x24, 235, 0 };
            seriaPort.Write(comando, 0, comando.Length);
            texto = "EXEMPLO DE IMPRESSAO\n";
            seriaPort.Write(texto);
            // Definir posição da impressão de texto.
            comando = new byte[] { 0x1B, 0x24, 235, 0 };
            seriaPort.Write(comando, 0, comando.Length);
            texto = "EXEMPLO DE IMPRESSAO\n";
            seriaPort.Write(texto);
            // Definir posição da impressão de texto.
            comando = new byte[] { 0x1B, 0x24, 235, 0 };
            seriaPort.Write(comando, 0, comando.Length);
            texto = "EXEMPLO DE IMPRESSAO\n";
            seriaPort.Write(texto);
            // Definir posição da impressão de texto.
            comando = new byte[] { 0x1B, 0x24, 235, 0 };
            seriaPort.Write(comando, 0, comando.Length);
            texto = "EXEMPLO DE IMPRESSAO\n";
            seriaPort.Write(texto);
            // Definir posição da impressão de texto.
            comando = new byte[] { 0x1B, 0x24, 235, 0 };
            seriaPort.Write(comando, 0, comando.Length);
            texto = "EXEMPLO DE IMPRESSAO\n";
            seriaPort.Write(texto);

            // Mandando Impressão em Modo Página.
            comando = new byte[] { 0x0C };
            seriaPort.Write(comando, 0, comando.Length);
            comando = new byte[] { 0x1D, 0x56, 0x48, 0x1D, 0x56, 0x42, 0x00 };
            seriaPort.Write(comando, 0, comando.Length);
        }
    }
}
